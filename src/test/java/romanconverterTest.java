import org.junit.Test;
import roman.RomanConverter;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class romanconverterTest {
    @Test
    public void test1(){
        String a = "X";
        RomanConverter rc1 = new RomanConverter();
        assertEquals(rc1.fromRoman(a), 10);
    }
    @Test
    public void test2(){
        int b = 40;
        RomanConverter rc2 = new RomanConverter();
        assertEquals(rc2.toRoman(b),"XL");
    }

    @Test
    public void test3(){
        int c = 48;
        RomanConverter rc3 = new RomanConverter();
        assertEquals(rc3.toRoman(c),"XLVIII");

    }

    @Test(expected=IllegalArgumentException.class)
    public void test4(){
        RomanConverter rc4 = new RomanConverter();
        int min = 0;
        fail(rc4.toRoman(min));
    }

    @Test(expected=IllegalArgumentException.class)
    public void test5(){
        RomanConverter rc5 = new RomanConverter();
        int max = 4000;
        fail(rc5.toRoman(max));
    }

    @Test(expected=IllegalArgumentException.class)
    public void test6() {
        RomanConverter rc6 = new RomanConverter();
        String notroman = "ABC";
        rc6.fromRoman(notroman);
        fail();
    }
}
